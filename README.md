# AdsOntology

## Intro
This is the scripts/tool for building advertistment ontology.
It includes scraping data, extracting keywords, and calculating TF-IDF.

## License

AdsOntology is released as open source software under the GPLv3 license.

Copyright (C) 2017  Sheng-Fu Chang

AdsOntology is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License Version 3 as published by
the Free Software Foundation.

AdsOntology is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with AdsOntology. See LICENSE for details.  If not, see <http://www.gnu.org/licenses/>.
