import MySQLdb
import MySQLdb.cursors


class AdsDB:

    def __init__(self):
        # changes to your DB config
        self.__db = MySQLdb.connect(host="localhost",
                                    port=3306,
                                    user="root",
                                    passwd="",
                                    db="adsontology",
                                    cursorclass=MySQLdb.cursors.DictCursor)
        self.__db.set_character_set('utf8mb4')

    def insert(self, string, _name, _link, _img, _content, _category):
        __cur = self.__db.cursor()
        result = __cur.execute(
            string, (_name, _link, _img, _content, _category))
        self.__db.commit()
        __cur.close()
        return result

    def select(self, string):
        __cur = self.__db.cursor()
        __cur.execute(string)
        self.__db.commit()
        rows = __cur.fetchall()
        __cur.close()
        return rows

    def insert_keyword(self, string, _word, _freq, _ads_id):
        __cur = self.__db.cursor()
        result = __cur.execute(string, (_word, _freq, _ads_id))
        self.__db.commit()
        __cur.close()
        return result

    def update(self, string):
        __cur = self.__db.cursor()
        result = __cur.execute(string)
        self.__db.commit()
        __cur.close()
        return result
