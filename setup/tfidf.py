# tfidf.py
# calculate category_keyword TF-IDF value and update DB value
# Input: category_keywords

import math
import db

CATEGORY_NUM = 6
ADSDB = db.AdsDB()

for i in range(CATEGORY_NUM):
    QUERY_STR = """
        SELECT id, word, freq FROM category_keywords
        WHERE category_id = %s
        ORDER BY id ASC;
    """

    QUERY_STR = QUERY_STR % i
    rows = ADSDB.select(QUERY_STR)

    for row in rows:
        category_keyword_id = row['id']
        word = row['word']
        word_freq_n = int(row['freq'])

        print('Working on id: %d' % category_keyword_id)

        QUERY_STR = """
            SELECT SUM(freq) AS sum FROM category_keywords
            WHERE category_id = %s
            ORDER BY id ASC;
        """

        QUERY_STR = QUERY_STR % i
        rows = ADSDB.select(QUERY_STR)
        word_freq_sum = int(rows[0]['sum'])

        QUERY_STR = """
            SELECT COUNT(category_id) AS count FROM category_keywords
            WHERE word = '%s' AND category_id <= %s
            ORDER BY id ASC;
        """

        QUERY_STR = QUERY_STR % (word, CATEGORY_NUM - 1)
        rows = ADSDB.select(QUERY_STR)
        word_count_d = int(rows[0]['count'])

        TF = word_freq_n / word_freq_sum
        IDF = math.log((CATEGORY_NUM / word_count_d), 10)
        TFIDF = TF * IDF

        QUERY_STR = """
            UPDATE category_keywords
            SET `tf` = %s, `idf` = %s, `tf-idf` = %s
            WHERE `id` = %s;
        """

        QUERY_STR = QUERY_STR % (TF, IDF, TFIDF, category_keyword_id)
        ADSDB.update(QUERY_STR)

print('Finish')
