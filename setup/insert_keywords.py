# insert_keywords.py
# insert extracted keyword to DB
# Input: extracted keywords

# Important Note: this script only insert `extracted` keyword, it means that we don't sort them by ads category
# You have to build category_keywords table by yourself. We have an example as follow:
# TABLE|COLUMN
# category_keyword | id, word, freq, category_id
#
# SQL command:
# INSERT INTO category_keywords (word, freq, category_id)
# SELECT `ads_keywords`.`word`, SUM(`ads_keywords`.`freq`), `ads`.`category_id` FROM `ads`
# INNER JOIN `ads_keywords`
# WHERE `ads_keywords`.`ads_id` = `ads`.`id` AND `ads`.`category_id` = 0
# GROUP BY `ads_keywords`.`word`
# ORDER BY `ads_keywords`.`id`
# LIMIT 30

import codecs
import os
import re
import db

ADSDB = db.AdsDB()
KEYWORDS_FREQ_PATH = './freq'

for file in os.listdir(KEYWORDS_FREQ_PATH):
    ADS_ID = file.split('.txt')[0]
    f = codecs.open('%s/%s' % (KEYWORDS_FREQ_PATH, file), 'r', encoding='utf-16-le', errors='replace')
    data = f.read()
    filterd_keywords = re.findall('.*\(N\)\t\d+', data)
    if not filterd_keywords:
        continue
    print('Working on %s...' % file)

    for keyword_data in filterd_keywords:
        keyword, freq = keyword_data.split('\t')
        keyword = keyword.replace('(N)', '')

        QUERY_STR = """
            INSERT INTO ads_keywords (word, freq, ads_id) 
            VALUES (%s, %s, %s);
        """

        try:
            ADSDB.insert_keyword(QUERY_STR, keyword, freq, ADS_ID)
        except:
            print('Error!\nQUERY_STR = \n%s\n%s, %s, %s' % (QUERY_STR, keyword, freq, ADS_ID))
            exit()

print('Finish!')
