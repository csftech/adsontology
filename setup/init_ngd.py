from urllib.parse import quote
import lxml.html
import math
import time
import requests
import db

CATEGORY_NUM = 6


def google_search(string):
    url = 'https://www.google.com.tw/search?hl=zh-tw&q=%s'
    search_str = string.split(' ')

    if len(search_str) == 2:
        url = 'https://www.google.com.tw/search?hl=zh-tw&q=%s+%s' % (
            quote(search_str[0]), quote(search_str[1]))
    else:
        url = url % quote(search_str[0])

    # print(url)
    hdr = {
        'User Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3202.94 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
        'DNT': '1',
        'Upgrade-Insecure-Requests': '1',
        'Connection': 'keep-alive'
    }
    sess = requests.Session()
    req = sess.get(url, headers=hdr)
    etree = lxml.html.fromstring(req.text)
    # magic
    index_num = etree.xpath(
        './/div[@id="resultStats"]')[0].text_content().split(' ')[1].replace(',', '')
    return index_num


def initialize():
    ADSDB = db.AdsDB()

    # initialize ontology search engine index num
    # calculate ontology's NGD first, it could increase whole process performance

    QUERY_STR = """
        SELECT * FROM ads_ontology WHERE `index_num` = 0 ORDER BY id ASC;
    """

    rows = ADSDB.select(QUERY_STR)
    for row in rows:
        ads_ontology_id = row['id']
        word = row['word']
        index = google_search(word)
        time.sleep(1)

        UPDATE_STR = """
            UPDATE `ads_ontology`
            SET `index_num` = %s
            WHERE `id` = %s;
        """

        UPDATE_STR = UPDATE_STR % (index, ads_ontology_id)
        print(UPDATE_STR)
        ADSDB.update(UPDATE_STR)

    print('ads_ontology index initialized.')

    QUERY_KEYWORDS_STR = """
        SELECT id, word FROM ads_keywords WHERE `index_num` = 0 ORDER BY id ASC;
    """

    rows = ADSDB.select(QUERY_KEYWORDS_STR)
    for row in rows:
        ads_keyword_id = row['id']
        word = row['word']
        index = google_search(word)
        time.sleep(1)

        UPDATE_STR = """
            UPDATE `ads_keywords`
            SET `index_num` = %s
            WHERE `id` = %s;
        """
        UPDATE_STR = UPDATE_STR % (index, ads_keyword_id)
        print(UPDATE_STR)
        ADSDB.update(UPDATE_STR)

    print('ads_keywords index initialized.')

initialize()
