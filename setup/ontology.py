# ontology.py
# build ads_ontology
# Input: category_keywords

import db

CATEGORY_NUM = 6
ONTOLOGY_ELEMENTS_NUM = 4

DROP_STR = """
    DROP TABLE IF EXISTS `ads_ontology`;
"""
CREATE_STR = """
    CREATE TABLE IF NOT EXISTS `ads_ontology` (
    `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `word` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
    `tf-idf` double NOT NULL,
    `category_id` bigint(20) UNSIGNED NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    KEY `ads_ontology_category_id_fk` (`category_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
"""
ALTER_STR = """
    ALTER TABLE `ads_ontology`
    ADD CONSTRAINT `ads_ontology_category_id_fk` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
"""

ADSDB = db.AdsDB()
ADSDB.update(DROP_STR)
ADSDB.update(CREATE_STR)
ADSDB.update(ALTER_STR)

INSERT_STR = """
    INSERT INTO ads_ontology (`word`, `tf-idf`, `category_id`)
    SELECT `category_keywords`.`word`, `category_keywords`.`tf-idf`, `category_keywords`.`category_id` FROM `category_keywords`
    WHERE `category_keywords`.`category_id` = %d
    ORDER BY `category_keywords`.`tf-idf` DESC
    LIMIT %d
"""

for i in range(CATEGORY_NUM):
    INSERT_STR = """
        INSERT INTO ads_ontology (`word`, `tf-idf`, `category_id`)
        SELECT `category_keywords`.`word`, `category_keywords`.`tf-idf`, `category_keywords`.`category_id` FROM `category_keywords`
        WHERE `category_keywords`.`category_id` = %d
        ORDER BY `category_keywords`.`tf-idf` DESC
        LIMIT %d
    """
    INSERT_STR = INSERT_STR % (i, ONTOLOGY_ELEMENTS_NUM)
    ADSDB.update(INSERT_STR)
