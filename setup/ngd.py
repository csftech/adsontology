# ngd.py
# calculate NGD value
# Input: ads_ontology, ads_keywords

from urllib.parse import quote
import lxml.html
import math
import time
import requests
import db

CATEGORY_NUM = 6


def google_search(string):
    url = 'https://www.google.com.tw/search?hl=zh-tw&q=%s'
    search_str = string.split(' ')

    if len(search_str) == 2:
        url = 'https://www.google.com.tw/search?hl=zh-tw&q=%s+%s' % (
            quote(search_str[0]), quote(search_str[1]))
    else:
        url = url % quote(search_str[0])

    #print(url)
    hdr = {
        'User Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3202.94 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
        'DNT': '1',
        'Upgrade-Insecure-Requests': '1',
        'Connection': 'keep-alive'
    }
    sess = requests.Session()
    req = sess.get(url, headers=hdr)
    etree = lxml.html.fromstring(req.text)
    # magic
    index_num = etree.xpath(
        './/div[@id="resultStats"]')[0].text_content().split(' ')[1].replace(',', '')
    return index_num

def initialize():
    ADSDB = db.AdsDB()

    # initialize ontology search engine index num
    # calculate ontology's NGD first, it could increase whole process performance

    QUERY_STR = """
        SELECT * FROM ads_ontology ORDER BY id ASC;
    """

    rows = ADSDB.select(QUERY_STR)
    for row in rows:
        ads_ontology_id = row['id']
        word = row['word']
        index = google_search(word)

        UPDATE_STR = """
            UPDATE `ads_ontology`
            SET `index_num` = %s
            WHERE `id` = %s;
        """

        UPDATE_STR = UPDATE_STR % (index, ads_ontology_id)
        print(UPDATE_STR)
        ADSDB.update(UPDATE_STR)

    print('ads_ontology index initialized.')

# Main Part - Calculate NGD and Characteristic vector of each news

ADSDB = db.AdsDB()
QUERY_ADS_STR = """
    SELECT `ads`.`id` FROM `ads`
    WHERE `ads`.`category_id` <= %s
    ORDER BY `ads`.`id` ASC
"""
QUERY_ADS_STR = QUERY_ADS_STR % (CATEGORY_NUM - 1)

ADS_ROWs = ADSDB.select(QUERY_ADS_STR)

for row in ADS_ROWs:
    ads_id = row['id']

    QUERY_KEYWORDS_STR = """
        SELECT `ads_keywords`.`word` FROM `ads_keywords`
        WHERE `ads_keywords`.`ads_id` = %s
        ORDER BY `ads_keywords`.`id` ASC
    """
    QUERY_KEYWORDS_STR = QUERY_KEYWORDS_STR % ads_id

    KW_ROWS = ADSDB.select(QUERY_KEYWORDS_STR)
    m = len(KW_ROWS)
    V = [0, 0, 0, 0, 0, 0]
    count = 0
    for kw_row in KW_ROWS:
        ads_word = kw_row['word']
        for i in range(CATEGORY_NUM):
            QUERY_ONTOLOGY_STR = """
                SELECT `word`, `index_num` FROM `ads_ontology`
                WHERE `category_id` = %s
                ORDER BY `id` ASC
            """
            QUERY_ONTOLOGY_STR = QUERY_ONTOLOGY_STR % i
            ontology_rows = ADSDB.select(QUERY_ONTOLOGY_STR)

            for row in ontology_rows:
                ontology_word = row['word']
                log_x = math.log(int(row['index_num']), 2)
                log_y = math.log(int(google_search(ads_word)), 2)
                time.sleep(1)
                log_xy = math.log(int(google_search('%s %s' % (ontology_word, ads_word))), 2)
                time.sleep(1)
                log_N = math.log(25270000000, 2)

                ngd = (max(log_x, log_y) - log_xy) / (log_N - min(log_x, log_y))
                V[i] = V[i] + ngd
                count += 1
                print(count)

    print(V)
    exit()