# extract_keywords.py
# extract keyword
# Input: Ads in DB

import os
import db

ADSDB = db.AdsDB()
QUERY_STR = """
    SELECT id, content FROM ads ORDER BY id;
"""


def check_tuple(db_obj, _id):
    QUERY_STR = """
        SELECT ads_id FROM keywords WHERE ads_id = %s;
    """
    QUERY_STR = QUERY_STR % (_id)
    rows = db_obj.select(QUERY_STR)
    if rows:
        return 1
    return 0


# download ads
rows = ADSDB.select(QUERY_STR)
for row in rows:
    if not check_tuple(ADSDB, row['id']):
        filename = './ads/%s.txt' % row['id']
        f = open(filename, 'w')
        f.write(row['content'].encode('big5', errors='ignore').decode('big5'))
        f.close()
    continue

# extract keywords(waiting until it finish)
print('\rCKIPClient is working.')
os.system('CKIPClient.exe ckipsocket.propeties ads keywords')
print('\rCKIPClient is done. Look file in keywords folder.')

# calculate keyword frequency
print('\rCountWordFreq is working.')

for file in os.listdir('./keywords'):
    filein = './keywords/' + file
    fileout = './freq/' + file
    os.system('CountWordFreq %s %s' % (filein, fileout))

print('\rCountWordFreq is done. Look file in freq folder.')
