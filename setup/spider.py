# spider.py
# Input: a file contains ad urls(seperated by a line break, \n)
# only compatible with https://tw.ysm.emarketing.yahoo.com/

from urllib.request import urlopen
import lxml.html
import db

QUERY_STR = """
    INSERT INTO ads (name, link, img, content, category_id) 
    VALUES (%s, %s, %s, %s, %s);
"""


def check_category(text):
    category_fixed = text.split(' ＞ ')[0]
    return {
        '居家裝潢': 0,
        '休閒旅遊': 1,
        '交通運輸': 2,
        '商業學術': 3,
        '生活服務': 4,
        '醫療保健': 5,
        '工商服務': 6
    }[category_fixed]


# default filename, rename it if you use different filename.
with open('./ads.txt', 'r') as f:
    ADSDB = db.AdsDB()
    URLS = f.readlines()
    URLS_LENGTH = len(URLS)

    for idx, url in enumerate(URLS, start=1):
        link = url.strip()
        html = lxml.html.parse(urlopen(link))
        root = html.getroot()
        name = ''
        img = ''
        content = ''
        category = 0

        e_title = root.find('.//title')
        if e_title is not None:    # check whether the ad has been removed or not
            text_list = e_title.text.split(' - ')
            name = text_list[0]
            img = root.find('.//img[@id="main"]').attrib['src']

            # crawl the content
            spans = root.xpath('.//div[@id="fancybox"]/span')
            if len(spans) == 2:
                content = spans[1].text_content()
            else:
                content = spans[0].text_content()
            content = content[:-5]    # remove last five characters 'close'

            category = check_category(text_list[2])

            ADSDB.insert(QUERY_STR, name, link, img, content, category)
            percent = '\rpercentage: %d%%' % (idx / URLS_LENGTH * 100)
            print(percent, end='')
